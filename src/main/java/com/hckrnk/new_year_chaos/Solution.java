package com.hckrnk.new_year_chaos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

    static void minimumBribes(int[] q) {
        List<Integer> q2 = new ArrayList();
        for (int i = 1; i < q.length + 1; i++) {
            q2.add(i);
        }

        int bribes = 0;
        for (int i = 0; i < q.length; i++) {
            if (q2.get(i) != q[i]) {

                int from = q[i] - 1 > i ? q[i] - 1 : i;
                int to = q[i] - 1 > i ? i : i + 1;

                int bribeCount = Math.abs(from - to);
                if (bribeCount > 2) {
                    System.out.println("Too chaotic");
                    return;
                }
                q2.add(to, q2.remove(from));
                bribes = bribes + bribeCount;
            }
        }
        System.out.println(bribes);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] q = new int[n];

            String[] qItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int qItem = Integer.parseInt(qItems[i]);
                q[i] = qItem;
            }

            minimumBribes(q);
        }

        scanner.close();
    }
}
